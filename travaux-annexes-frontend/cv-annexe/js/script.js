document.addEventListener('DOMContentLoaded', function() {
   
   
   
   let setApp = function() {



       // Fonction qui facilite l'utilisation de query selector
       function html(cssSelector) {
         return document.querySelector(cssSelector);
      }
   


    let titleName = 'AlExiS ServEt';
    
    

    let h1ContainerDiv = html('#h1-container-div');
      
    let h2ContainerDiv = html('#h2-container-div');
   // Miroir
   let h2ContainerDivMirror = html('#h2-container-div-mirror');
       h2ContainerDivMirror.classList.add('mirror');
       h2ContainerDivMirror.classList.add('transparence');



    

    let marketingAndCom = 'Assistant marketing & Communication';
    // Miroir
    let marketingAndComMirror = 'Assistant marketing & Communication';



    
    // Fonction permettant de placer chaque lettre d'une phrase dans une span et lui attribut un ID
    function addDiv( sentence, domElement, parentType, rotateOption = false, attribut = 'id', name = '' ) {
    
         let array = sentence.split('');
    
           let i = 0;
    
             for( let letter of array ) {
    
                i++;
    
               let div = document.createElement('div');
                   div.setAttribute(attribut, name + 'div-' + parentType + '-' + i);
                
                   div.innerHTML = letter;
    
                  domElement.appendChild(div);
    
                  if(rotateOption === true ) {
                      div.className = 'div-hide';
                  }
    
                }
        
    }
    



    
    // Fonction permettant de faire pivoter les lettres de la chaîne les unes après les autres 
    // afin de les monter
    
    function showAnimation() {
       
       let counter = 0;
       
       if(counter < 35) {
         
         // Active l'animation avec le nombre de secondes de retard indiqué
         setTimeout(function() {

          let timeAnimation = setInterval(function() {
             
             if(counter >= 35) {
                
                clearInterval(timeAnimation);
               }
               else {

                  counter++; 
                  
                  let divShow = html('#div-h2-' + counter);
                  divShow.className = 'div-show';
                  // Miroir
                  let divShowMirror = html('#mirror-div-h2-' + counter);
                  divShowMirror.className = 'div-show';
                  divShowMirror.classList.add('tilt');

               }
                  
            }, 60);  
         
         }, 2200);
            
         }
       
      }



      

      // Fonction permettant d'injecter chaque lettre d'un mot dans une 'div'
      // et de la rattacher à un container.

      function setTitle(word, domContainer) {

         wordSplit = word.split('');
         
         let iteration = 0;
    
         for( let wordLetter of wordSplit ) {

            iteration++;

           let div = document.createElement('div');
               div.setAttribute('id', 'div-title-' + iteration);

               div.className = 'invisible';

               div.innerHTML = wordLetter;
               
              domContainer.appendChild(div);

   

            }


      }


      // *****  Animation du header
      function headerAnimation() {

             let iCounter = 0;
            
             setTimeout(function() {
              
         

           let headerTitleInterval = setInterval(function() {
 
                  if(iCounter < 13) {
                     iCounter++;

                       let headerDivShow = html('#div-title-' + iCounter);
                           headerDivShow.className = 'visible';
                       

                  } 
                  else {
                     clearInterval(headerTitleInterval);
                  }

             }, 100);

            }, 300);

            

      }

      // On pose le titre de la page dans le DOM
      setTitle(titleName, h1ContainerDiv);
      // On active l'animation d'apparition du header dans le DOM
      headerAnimation();
      // On crée la phrase dans le dom
      
      addDiv( marketingAndCom, h2ContainerDiv, 'h2', true );
      //Miroir
      addDiv( marketingAndComMirror, h2ContainerDivMirror, 'h2', true, 'id', 'mirror-' );


      // On active l'animation sur le premier mot du h2
      showAnimation();

     



 // Ferumeture de setApp()
 }

   
   setApp();

   
//  Fermerue de l'écouteur DOMContentLoaded 
});