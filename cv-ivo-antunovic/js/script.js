document.addEventListener('DOMContentLoaded', function() {
    
   
   // Fonction qui facilite l'utilisation de query selector
   function html(cssSelector, selectorAllOption = false) {
      if(selectorAllOption === false) {
         return document.querySelector(cssSelector);
      }

      if(selectorAllOption === true ) {
         return document.querySelectorAll(cssSelector);
      }
   }
   
   


   /**
    * *************************************
    *  Variables pour le"HEADER"
    *  ************************************
    *  */ 
   let h1 = html('#h1'),
      h1_content = 'Ivo Antunovic';
   
   let h2 = html('#h2'),
       h2_content = '< DÉVELOPPEUR WEB FULL STACK / >';
   
          // Images du header
          let inkDrop = html('.ink-drop-container');
          let blackCloud = html('#black-cloud');

 
          
    /**
    * *******************************
    *      Variable pour le MAIN
    * *******************************
    */
   
   let sub_main_container_1 = html('.sub-main-container-1');

   // Variables pour la partie "Réalisation/web"
   let achievements_h3 = html('#achievements-h3'),
       achievements_h3_content = 'Réalisations/web';

   let achievements_h4_1 = html('#achievements-h4-1'),
       achievements_h4_content_1 = 'CV — CV Web Responsive';

    let achievements_p_1 = html('#achievements-p-1'),
    achievements_p_content_1 = 'Réalisation d’un CV sous la forme d’un site web statique, monopage, adapté à plusieurs supports: mobile, tablette et format bureau .';


   let achievements_h4_2 = html('#achievements-h4-2'),
       achievements_h4_content_2 = 'Map Events — Application d’événements cartographiés';

   let achievements_p_2 = html('#achievements-p-2'),
       achievements_p_content_2 = 'Réalisation d’une application permettant de créer des événements  culturels, cartographiés via un service web . ';
   


       
   // Variables pour la partie "Expérience"
   let experience_h3 = html('#experience-h3');
   let experience_h3_content = 'Expérience';

   let experience_h4 = html('#experience-h4');
   let experience_h4_content = 'Communauté de Communes Roussillon Conflent';

   let experience_h5 = html('#experience-h5');
   let experience_h5_content = " Agent d’animation —  Ille-sur-Têt, Rodès, Millas 2018 - 2019";

   let experience_p1 = html('#experience-paragraph-1');
   let experience_p1_content = 'Réalisation de projets pédagogiques en concertation avec  la direction.';

   let experience_p2 = html('#experience-paragraph-2');
   let experience_p2_content = 'Élaboration de projets d’activité en accord avec le cahier des charges. ';



   // Variables pour les sections a faire apparaitre
   let section_containers = html('.section-container', true);

    let studies_section = html('#studies-section');

    let addresses_section= html('#addresses-section');

    let programming_languages_section = html('#programming-languages-section');

    let frameworks_section = html('#frameworks-section');
    
    let production_tools_section = html('#production-tools-section');

    let foreign_languages_section = html('#foreign-languages-section');
    


       /*
       * 
       * Fonction permettant d'animer
       * l'écriture des paragrpahes dans le DOM 
       */
      function autoWriting( sentence, domElement, writingSpeed, animationTiming) {
         
         let letters = sentence.split('');
         
         let i = 0;
         
         setTimeout(function() {
            setInterval(function() {
               
               if(i <= letters.length - 1) {
                  
                  domElement.innerHTML += letters[i];
                  
                  i++;
                  
               }
               
            }, writingSpeed);
         }, animationTiming); 
      }
      





      
      // Animation faisant apparaître le prénom et le nom dans le DOM
      function inkDropAnimation(animationTiming) {
           setTimeout(function() {
                   inkDrop.className = 'none';     
                   blackCloud.classList.remove('none');
      
                   setTimeout(function() {
                      h1.classList.replace('invisible', 'visible');
                   }, 500);
      
      
      
           }, animationTiming);
      }
    
      




/*
****************************************
* ||| ANIMATION POUR LE HEADER DU CV |||
****************************************
*/

function headerAnimation(){

   inkDropAnimation(1300);
   autoWriting(h2_content, h2, 70, 3400);

}






/*
 ****************************************
 * |||| ANIMATION POUR LE MAIN DU CV ||||
 ****************************************
 */
function mainAnimationWriting()
{

   // Éléments qui apparaissent avec l'écriture automatique

   showContainer(sub_main_container_1, 6000);

   autoWriting(achievements_h3_content, achievements_h3, 20, 7500);

   autoWriting(achievements_h4_content_1, achievements_h4_1, 20, 7500);
   autoWriting(achievements_p_content_1, achievements_p_1, 20, 7500);

   autoWriting(achievements_h4_content_2, achievements_h4_2, 20, 7500);
   autoWriting(achievements_p_content_2, achievements_p_2, 20, 7500);
   autoWriting(experience_h3_content, experience_h3, 20, 7500);
   autoWriting(experience_h4_content, experience_h4, 20, 7500);
   autoWriting(experience_h5_content, experience_h5, 20, 7500);

   autoWriting(experience_p1_content, experience_p1, 20, 7500);
   autoWriting(experience_p2_content, experience_p2, 20, 7500);


   // Éléments qui apparaissent d'un bloc
   for(section_container of section_containers) {
      showContainer(section_container, 7500);
   }

   showSection(studies_section, 8700);
   showSection(addresses_section, 10700);
   showSection(programming_languages_section, 10800);
   showSection(frameworks_section, 11200);
   showSection(production_tools_section, 11900);
   showSection(foreign_languages_section, 12500);

}


function showContainer(container, animationTiming) {
 
    setTimeout(function() {
         container.classList.add('border-shadow');
    }, animationTiming);

}


// Animation permettant l'apparition des sections invisibles par default
function showSection($section, animationTiming) {
   setTimeout(function() {
      $section.classList.replace('invisible', 'visible');
   },  animationTiming);
}






/**
 * Utilisation des animation dans le DOM
 */



// On active l'animation dans le header 
headerAnimation();

// On active l'animation dans le main
mainAnimationWriting();




// FERMETURE DU DOMContentLoaded
})